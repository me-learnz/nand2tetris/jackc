# jackc

[![pipeline status](https://gitlab.com/me-learnz/nand2tetris/jackc/badges/master/pipeline.svg)](https://gitlab.com/me-learnz/nand2tetris/jackc/pipelines)
[![codecov](https://codecov.io/gl/nand2tetris/jackc/branch/master/graph/badge.svg)](https://codecov.io/gl/nand2tetris/jackc)

A compiler translating a Jack language to a virtual machine language of a Hack computer developed in a course [From NAND to Tetris: Building a Modern Computer From First Principles](http://www.nand2tetris.org).
