//! Tokenizer reads a source file and outputs a stream of tokens
//!
//! Currently the source file needs to contain only ASCII characters (1 byte
//! per character).
//!
//! Tokenizes makes sure that all tokens are valid (i.e. using only allowed
//! characters, etc.)

use std::io::{self, Read, Bytes};
use std::fs::File;
use std::path::Path;
use std::result;

use types::Token;
use types::TokenType::*;
use xml_writer::XMLWriter;

/// Simple result type
pub type Result<T> = result::Result<T, String>;

/// Possible variants of symbols
static SYMBOLS: [char; 19] = [
        '{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*', '/', '&',
        '|', '<', '>', '=', '~',
];

/// Possible keywords
static KEYWORDS: [&'static str; 21] = [
    "class",   "method",   "function",  "constructor",
    "int",     "boolean",  "char",      "void",
    "var",     "static",   "field",     "let",
    "do",      "if",       "else",      "while",
    "return",  "true",     "false",     "null",
    "this",
];

/// Maximum size of a token in bytes (beware of long strings!)
const BUF_SIZE: usize = 1024;  // size in bytes

/// What is being processed now
#[cfg_attr(test, derive(Debug, PartialEq))]
enum In {
    /// When inside double quotes
    StringConst,
    /// When processing integer
    Integer,
    /// Wither a keyword or an identifier
    Word,
    /// When inside a 1-line comment
    CommentLine,
    /// When inside a multi-line comment (applies to an API comment as well)
    CommentMult,
    /// Ending a multi-line comment (i.e. after '*' when inside a comment)
    EndingCommentMult,
    /// When possibly a comment or integer (i.e. after `/`)
    EnteringComment,
    /// Default state
    Plain,
}

/// `Tokenizer` operates as an iterator over tokens detected in a source file.
pub struct Tokenizer {
    /// Iterator over the source file, one byte at a time
    src_file_bytes: Bytes<File>,

    /// Buffer to store bytes of an unprocessed token
    buf: [u8; BUF_SIZE],

    /// Current position where to store a next byte
    buf_idx: usize,

    /// Vector of `Token`s to be returned by the iterator.
    token_queue: Vec<(Result<Token>, u32)>,

    /// Keeps track of what is being processed now
    whereami: In,

    /// Keeps track of a current line number
    line_num: u32,

    /// Optional writer to output the token stream into an XML file
    xml_writer: Option<XMLWriter>,
}

impl Tokenizer {
    /// Open a given `src_file` and return a new Tokenizer
    pub fn new(src_file: &Path, xml_writer: Option<XMLWriter>)
        -> io::Result<Tokenizer>
    {
        let src_file_bytes = File::open(src_file)?.bytes();

        let mut tokenizer = Tokenizer {
            src_file_bytes,
            buf: [0; BUF_SIZE],
            buf_idx: 0,
            token_queue: Vec::new(),
            whereami: In::Plain,
            line_num: 0,
            xml_writer: xml_writer,
        };

        if let Some(ref mut writer) = tokenizer.xml_writer {
            writer.write_nonterm_open("tokens")
                .expect("Error writing an opening tag `<tokens>` to a XML \
                        file.");
        }

        Ok(tokenizer)
    }

    /// Iterate over bytes of source file until token is found. Then return it.
    ///
    /// Also returns the line number where the token was found. Return `None`
    /// if the file is exhausted.
    pub fn advance(&mut self) -> Option<(Result<Token>, u32)> {
        // Iterate over bytes until a token is found
        while self.token_queue.is_empty() {
            let byte: u8 = match self.src_file_bytes.next() {
                Some(res) => match res {
                    Ok(byte) => byte,
                    Err(e) => return Some((Err(e.to_string()), self.line_num)),
                },
                None => {
                    if !self.buf_is_empty() {
                        return Some((Err(format!(
                            "Encountered an end of the source file with a \
                            non-empty buffer containing `{}`. Try inserting a \
                            newline at the end of the file.",
                            self.buf_flush())), self.line_num))
                    } else {
                        return None
                    }
                },
            };
            self.process_byte(byte);
        }

        match self.token_queue.pop() {
            Some(token_res) => Some(token_res),
            None => Some((Err(format!("No tokens in a file")), self.line_num)),
        }
    }

    /// Process one byte at a time
    fn process_byte(&mut self, byte: u8) {
        let c = byte as char;

        match self.whereami {
            In::Plain => {
                // If something is in the buffer, process it
                if !self.buf_is_empty() {
                    let s = self.buf_flush();
                    self.token_queue
                        .insert(0, (token_from_string(&s), self.line_num));
                }
                // Got whitespace
                if c.is_whitespace() {
                    if c == '\n' {
                        self.line_num += 1;
                    }
                // Got symbol
                } else if SYMBOLS.contains(&c) {
                    if c == '/' {  // Might go to comment
                        self.buf_push(byte);
                        self.whereami = In::EnteringComment;
                    } else {
                        self.token_queue
                            .insert(0, (Ok(Token::new(Symbol, &c.to_string())),
                                        self.line_num));
                    }
                // Got digit (entering an integer)
                } else if c.is_digit(10) {
                    if self.buf_is_empty() {
                        self.whereami = In::Integer;
                    }
                    self.buf_push(byte);
                // Got double quote
                } else if c == '"' {
                    self.whereami = In::StringConst;
                // Got a letter or '_'
                } else if c.is_alphabetic() || c == '_' {
                    if self.buf_is_empty() {
                        self.whereami = In::Word;
                    }
                    self.buf_push(byte);
                // Error
                } else {
                    self.token_queue
                        .insert(0, (Err(format!("Unknown character `{}` ", c)),
                                    self.line_num));
                }
            },
            In::EnteringComment => {
                let prev = self.buf_pop().expect("Tried to pop from an empty \
                                                 buffer");
                match c {
                    '/' => self.whereami = In::CommentLine,
                    '*' => self.whereami = In::CommentMult,
                    _ => {
                        self.buf_push(prev);
                        self.whereami = In::Plain;
                        self.process_byte(byte);
                    }
                }
            },
            In::CommentLine => {
                if c == '\n' {
                    self.line_num += 1;
                    self.whereami = In::Plain;
                }
            },
            In::CommentMult => {
                if c == '\n' {
                    self.line_num += 1;
                }
                if c == '*' {
                    self.whereami = In::EndingCommentMult;
                }
            },
            In::EndingCommentMult => {
                if c == '\n' {
                    self.line_num += 1;
                }
                if c == '/' {
                    self.whereami = In::Plain;
                } else {
                    self.whereami = In::CommentMult;
                }
            }
            In::StringConst => {
                if c == '\n' {
                    let flush = self.buf_flush();
                    self.token_queue.insert(0, (Err(
                        format!("Encountered a newline character when \
                                processing a string constant `{}`",
                                flush)), self.line_num));
                } else if c == '"' {
                    let flush = self.buf_flush();
                    self.token_queue
                        .insert(0, (Ok(Token::new(StringConst, &flush)),
                                    self.line_num));
                    self.whereami = In::Plain;
                } else {
                    self.buf_push(byte);
                }
            },
            In::Integer => {
                if c.is_digit(10) {
                    self.buf_push(byte);
                } else {
                    let flush = self.buf_flush();
                    let result = match flush.parse::<u16>() {
                        Ok(num) => Ok(Token::new(IntConst, &num.to_string())),
                        Err(_) => Err(format!("Error parsing integer `{}`",
                                              flush)),
                    };
                    self.token_queue.insert(0, (result, self.line_num));
                    // Resolve current character
                    self.whereami = In::Plain;
                    self.process_byte(byte);
                }
            },
            In::Word => {
                if c.is_alphanumeric() || c == '_' {
                    self.buf_push(byte);
                } else {
                    let flush = self.buf_flush();
                    self.token_queue
                        .insert(0, (token_from_string(&flush), self.line_num));
                    self.whereami = In::Plain;
                    self.process_byte(byte);
                }
            },
        }
    }

    /// Add byte to buffer[`buf_idx`] and increment `buf_idx`
    fn buf_push(&mut self, byte: u8) {
        self.buf[self.buf_idx] = byte;
        self.buf_idx += 1;
    }

    /// Read the last byte from `buf` and decrement `buf_idx`
    ///
    /// Returns None if `buf_idx` equal or less than 0
    fn buf_pop(&mut self) -> Option<u8> {
        if self.buf_idx > 0 {
            self.buf_idx -= 1;
            Some(self.buf[self.buf_idx])
        } else {
            None
        }
    }

    /// Reset `buf` and `buf_idx` to zeros and return `buf`'s content
    ///
    /// Returns a String made from the buffer bytes. The String is empty if the
    /// buffer has only zeros.
    fn buf_flush(&mut self) -> String {
        let mut s = String::with_capacity(self.buf_idx);
        for i in 0..self.buf_idx {
            s.push(self.buf[i] as char);
        }
        self.buf_idx = 0;
        s
    }

    /// Checks whether all bytes in `buf` before `buf_idx` are 0
    ///
    /// If `buf_idx` is 3, it only checks for `buf[0]`, `buf[1]` and `buf[2]`.
    /// Values after that don't matter because they cannot be read and will be
    /// overwritten by next `buf_push()`.
    fn buf_is_empty(&self) -> bool {
        for i in 0..self.buf_idx {
            if self.buf[i] != 0 {
                return false;
            }
        }
        return true;
    }
}

impl Iterator for Tokenizer {
    type Item = (Token, u32);

    /// Unwrap a result from advance() and pass it further in case of no errors
    ///
    /// Panics if there is some error when processing the tokens.
    /// Write to XML file, if `xml_writer` is specified.
    fn next(&mut self) -> Option<Self::Item> {
        match self.advance() {
            Some((Ok(token), line_num)) => {
                if let Some(ref mut writer) = self.xml_writer {
                    let tok_str = token.to_string();
                    let v: Vec<&str> = tok_str.splitn(2, ' ').collect();
                    writer.write_terminal(v[1], v[0])
                        .expect(&format!("Error writing a terminal tag for \
                                         `{}` to a XML file", tok_str));
                }
                Some((token, line_num))
            },
            Some((Err(e), line_num)) => panic!("Line {}: {}", line_num+1, e),
            None => {
                if let Some(ref mut writer) = self.xml_writer {
                    writer.write_nonterm_close("tokens")
                        .expect("Error writing a closing tag `</tokens>` to \
                                a XML file.");
                }
                None
            },
        }
    }
}

// Helper functions

/// Try to identify whether a string slice is a keyword, symbol or identifier.
///
/// Returns error if none of these.
fn token_from_string(s: &str) -> Result<Token> {
    if KEYWORDS.contains(&s) {
        return Ok(Token::new(Keyword, s))
    }

    if s.len() == 1 {
        let mut bytes = s.bytes();
        if let Some(b) = bytes.next() {
            let c = b as char;
            if SYMBOLS.contains(&c) {
                return Ok(Token::new(Symbol, &c.to_string()))
            }
        }
    }

    if let Ok(_) = is_id_valid(s) {
        return Ok(Token::new(Identifier, s))
    }

    Err(format!("Token `{}` is not a valid keyword, identifier or symbol", s))
}

/// Checks whether identifier is valid
///
/// Rules:
/// * Only letters (ASCII), digits (ASCII) and underscore
/// * Not starting with a digit
fn is_id_valid(id: &str) -> Result<()> {
    let char_is_valid = |c: char| {
        c.is_alphanumeric() || c == '_'
    };

    let mut id_chars = id.chars();

    match id_chars.next() {
        Some(c) => {
            if c.is_numeric() || !char_is_valid(c) {
                return Err(format!("First character is a digit or invalid"))
            }
        }
        None => return Err(format!("Empty identifier"))
    }

    for c in id_chars {
        if !char_is_valid(c) {
            return Err(format!("Invalid character `{}`", c))
        }
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use std::io::Write;
    use tempfile::NamedTempFile;
    use types::Token;
    use types::TokenType::*;
    use super::{Tokenizer, BUF_SIZE, is_id_valid, token_from_string, In};

    fn new_tokenizer(contents: &str) -> Tokenizer {
        let mut tmpfile = NamedTempFile::new()
            .expect("Error creating temp file.");
        write!(tmpfile, "{}", contents).expect("Error writing to temp file");
        Tokenizer::new(tmpfile.path(), None).expect("Error creating tokenizer")
    }

    #[test]
    fn buf() {
        let mut tokenizer = new_tokenizer("");
        let arr: [u8; 5] = [66, 97, 103, 114, 114];  // "Bagrr"

        assert!(tokenizer.buf_is_empty(), "Buffer is not empty after init");

        for i in arr.iter() {
            tokenizer.buf_push(*i);
        }
        assert_eq!(tokenizer.buf_idx, 5, "buff_add: index");
        assert_eq!(tokenizer.buf[0..5], arr, "buf_push: buffer data");
        assert!(!tokenizer.buf_is_empty(), "Buffer is empty after buff_add");

        assert_eq!(tokenizer.buf_pop().expect("didn't pop anything"), 114,
                   "buf_pop: wrong popped value");
        assert_eq!(tokenizer.buf[0..5], arr[0..5], "buf_pop: buffer data");

        let s = tokenizer.buf_flush();
        assert_eq!(s.capacity(), 4, "Wrong String capacity after flush");
        assert_eq!(s, String::from("Bagr"), "Wrong String after flush");
        assert_eq!(tokenizer.buf_idx, 0, "buff_flush: index");
        assert!(tokenizer.buf_is_empty(), "Buffer is not empty after flush");
    }

    #[test]
    fn buf_flush_empty() {
        let mut tokenizer = new_tokenizer("");
        let arr: [u8; BUF_SIZE] = [1; BUF_SIZE];

        tokenizer.buf = arr; // All bytes in buffer are occupied

        tokenizer.buf_flush();
        assert!(tokenizer.buf_is_empty(), "Buffer is not empty after flush");
    }

    #[test]
    fn tokens_plain() {
        // Text with only symbols, identifiers and keywords
        let contents = "class Main {\n    \
                            function void main() {\n        \
                                var String bagr;\n        \
                                let bagr += foo/x;\n        \
                                return;\n    \
                            }\n\
                        }\n";
        let tokenizer = new_tokenizer(contents);

        let expected: Vec<(Token, u32)> = vec![
            (Token::new(Keyword, "class"), 0),
            (Token::new(Identifier, "Main"), 0),
            (Token::new(Symbol, "{"), 0),
            (Token::new(Keyword, "function"), 1),
            (Token::new(Keyword, "void"), 1),
            (Token::new(Identifier, "main"), 1),
            (Token::new(Symbol, "("), 1),
            (Token::new(Symbol, ")"), 1),
            (Token::new(Symbol, "{"), 1),
            (Token::new(Keyword, "var"), 2),
            (Token::new(Identifier, "String"), 2),
            (Token::new(Identifier, "bagr"), 2),
            (Token::new(Symbol, ";"), 2),
            (Token::new(Keyword, "let"), 3),
            (Token::new(Identifier, "bagr"), 3),
            (Token::new(Symbol, "+"), 3),
            (Token::new(Symbol, "="), 3),
            (Token::new(Identifier, "foo"), 3),
            (Token::new(Symbol, "/"), 3),
            (Token::new(Identifier, "x"), 3),
            (Token::new(Symbol, ";"), 3),
            (Token::new(Keyword, "return"), 4),
            (Token::new(Symbol, ";"), 4),
            (Token::new(Symbol, "}"), 5),
            (Token::new(Symbol, "}"), 6),
        ];

        let it = tokenizer.zip(expected.into_iter());

        for (i, (tok, exp)) in it.enumerate() {
            assert_eq!(tok, exp, "Error comparing {}th token", i);
        }
    }

    #[test]
    fn tokens_complex() {
        // More complex text including all the `In`s
        let contents = "// Comment about symbols;\n\
                        /** This/function*is/ cool\n\
                         * \n\
                         * \n\
                         * ... or not */
                        function void main() {\n    \
                            var /* in*line! */ String bagr;\n    \
                            var 0123 foo; // Some number\n    \
                            let foo=9;
                            let bagr = \"barbaz\";\n    \
                            // Comment\n\
                            do (~bagr).bagruj(\"x\");\n    \
                            return;\n    \
                        }\n\
                        // End of file\n";
        let tokenizer = new_tokenizer(contents);

        let expected: Vec<(Token, u32)> = vec![
            (Token::new(Keyword, "function"), 5),
            (Token::new(Keyword, "void"), 5),
            (Token::new(Identifier, "main"), 5),
            (Token::new(Symbol, "("), 5),
            (Token::new(Symbol, ")"), 5),
            (Token::new(Symbol, "{"), 5),
            (Token::new(Keyword, "var"), 6),
            (Token::new(Identifier, "String"), 6),
            (Token::new(Identifier, "bagr"), 6),
            (Token::new(Symbol, ";"), 6),
            (Token::new(Keyword, "var"), 7),
            (Token::new(IntConst, "123"), 7),
            (Token::new(Identifier, "foo"), 7),
            (Token::new(Symbol, ";"), 7),
            (Token::new(Keyword, "let"), 8),
            (Token::new(Identifier, "foo"), 8),
            (Token::new(Symbol, "="), 8),
            (Token::new(IntConst, "9"), 8),
            (Token::new(Symbol, ";"), 8),
            (Token::new(Keyword, "let"), 9),
            (Token::new(Identifier, "bagr"), 9),
            (Token::new(Symbol, "="), 9),
            (Token::new(StringConst, "barbaz"), 9),
            (Token::new(Symbol, ";"), 9),
            (Token::new(Keyword, "do"), 11),
            (Token::new(Symbol, "("), 11),
            (Token::new(Symbol, "~"), 11),
            (Token::new(Identifier, "bagr"), 11),
            (Token::new(Symbol, ")"), 11),
            (Token::new(Symbol, "."), 11),
            (Token::new(Identifier, "bagruj"), 11),
            (Token::new(Symbol, "("), 11),
            (Token::new(StringConst, "x"), 11),
            (Token::new(Symbol, ")"), 11),
            (Token::new(Symbol, ";"), 11),
            (Token::new(Keyword, "return"), 12),
            (Token::new(Symbol, ";"), 12),
            (Token::new(Symbol, "}"), 13),
        ];

        let it = tokenizer.zip(expected.into_iter());

        for (i, (tok, exp)) in it.enumerate() {
            assert_eq!(tok, exp, "Error comparing {}th token", i);
        }
    }

    #[test]
    fn valid_id() {
        assert!(is_id_valid("BaGr").is_ok(), "ascii letters");
        assert!(is_id_valid("_").is_ok(), "underscore");
        assert!(is_id_valid("ba0gr1").is_ok(), "ascii letters + digits");
        assert!(is_id_valid("").is_err(), "empty");
        assert!(is_id_valid("0bagr").is_err(), "starting with a digit");
        assert!(is_id_valid("{bagr").is_err(), "starting with special char");
        assert!(is_id_valid("bag{r").is_err(), "special character inside");
        // Unicode:
        // assert!(is_id_valid("öbagr").is_err(), "starting with unicode");
        // assert!(is_id_valid("bagÄr").is_err(), "unicode inside");
    }

    #[test]
    fn keyword_identifier_symbol_identification() {
        let s = "class";
        assert_eq!(token_from_string(s).unwrap(),
                   Token::new(Keyword, "class"));

        let s = "bagr";
        assert_eq!(token_from_string(s).unwrap(),
                   Token::new(Identifier, "bagr"));

        let s = "0invalid";
        assert!(token_from_string(s).is_err());

        let s = ";";
        assert_eq!(token_from_string(s).unwrap(),
                   Token::new(Symbol, ";"));

        let s = "x";
        assert_eq!(token_from_string(s).unwrap(),
                   Token::new(Identifier, "x"));
    }

    #[test]
    fn enterings() {
        let mut tokenizer = new_tokenizer("/");
        tokenizer.advance();
        assert_eq!(tokenizer.whereami, In::EnteringComment,
                   "Wrong mode after `/`");

        let mut tokenizer = new_tokenizer("\"");
        tokenizer.advance();
        assert_eq!(tokenizer.whereami, In::StringConst,
                   "Wrong mode after `\"`");

        let mut tokenizer = new_tokenizer("//");
        tokenizer.advance();
        assert_eq!(tokenizer.whereami, In::CommentLine,
                   "Wrong mode after `//`");

        let mut tokenizer = new_tokenizer("/*");
        tokenizer.advance();
        assert_eq!(tokenizer.whereami, In::CommentMult,
                   "Wrong mode after `/*`");

        let mut tokenizer = new_tokenizer("/x");
        tokenizer.advance();
        assert_eq!(tokenizer.whereami, In::Word,
                   "Wrong mode after `/x`");

        let mut tokenizer = new_tokenizer("0");
        tokenizer.advance();
        assert_eq!(tokenizer.whereami, In::Integer,
                   "Wrong mode after `0`");

        let mut tokenizer = new_tokenizer("/**");
        tokenizer.advance();
        assert_eq!(tokenizer.whereami, In::EndingCommentMult,
                   "Wrong mode after `/**`");

        let mut tokenizer = new_tokenizer("/* */");
        tokenizer.advance();
        assert_eq!(tokenizer.whereami, In::Plain,
                   "Wrong mode after `/* */`");
    }

    #[test]
    fn tokens_comments() {
        // Text with identifiers and different comment types
        let contents = "// Comment\n\
                        bagr /* inline * comment */ foo /*\n\
                        multi-line\n      \
                        äÖÝá random¨\n \t\
                        stuff should be ignored*/bar\n\
                        /** This is an experimental API\n\
                         *\n\
                         * It does this baz\n\
                         */\n\
                        baz\n";

        let tokenizer = new_tokenizer(contents);

        let expected: Vec<(Token, u32)> = vec![
            (Token::new(Identifier, "bagr"), 1),
            (Token::new(Identifier, "foo"), 1),
            (Token::new(Identifier, "bar"), 4),
            (Token::new(Identifier, "baz"), 9),
        ];

        let it = tokenizer.zip(expected.into_iter());

        for (i, (tok, exp)) in it.enumerate() {
            assert_eq!(tok, exp, "Error comparing {}th token", i);
        }
    }

    // End of file without newline
    #[test]
    #[should_panic]
    fn no_ending_character() {
        let mut tokenizer = new_tokenizer("bagr");
        tokenizer.next();
    }

    #[test]
    fn string_const() {
        let tokenizer = new_tokenizer("let bagr = \"foobar\"");
        let tokens: Vec<Token> = tokenizer
            .map(|tok| tok.0).collect();

        assert_eq!(tokens.last(),
                   Some(&Token::new(StringConst, "foobar")),
                   "Error getting StringConst from `let bagr = \"foobar\"`");

        let mut tokenizer = new_tokenizer("(\"SOME LONGER STRING?\"\n");
        tokenizer.next();
        assert_eq!(tokenizer.next().expect("got None").0,
                   Token::new(StringConst, "SOME LONGER STRING?"));
    }

    #[test]
    #[should_panic]
    fn string_const_err() {
        let mut tokenizer = new_tokenizer("\"multi\nbar\"");
        tokenizer.next();
    }

    #[test]
    fn integers() {
        let mut tokenizer = new_tokenizer("0123 ");
        assert_eq!(tokenizer.next().expect("got None").0,
                   Token::new(IntConst, "123"));
        let mut tokenizer = new_tokenizer("0 ");
        assert_eq!(tokenizer.next().expect("got None").0,
                   Token::new(IntConst, "0"));
    }
}
