//! XML Writer module tailored to the needs of a Jack compiler
//!
//! Writes non-terminal element of type XXX as indented blocks:
//!
//! <XXX>
//!     XXX's body
//! </XXX>
//!
//! Terminals of type XXX are non-indented one-liners:
//!
//! <XXX> terminal </XXX>
//!
//! Together they might look like this:
//!
//! <XXX>
//!     <YYY> terminal </YYY>
//!     (rest of non-terminal's body...)
//! </XXX>

use std::io;
use std::io::Write;
use std::fs::File;
use std::path::Path;

/// What will be used for indenting XML nodes
static INDENT_STR: &'static str = "  ";

/// XML writer
pub struct XMLWriter {
    file: File,
    indent_level: u8,
    indent_str: &'static str,
}

impl XMLWriter {
    /// Create a new XMLWriter with no specified files
    pub fn new(file_path: &Path) -> io::Result<XMLWriter>
    {
        let file = File::create(file_path)?;
        Ok(XMLWriter { file, indent_level: 0, indent_str: INDENT_STR })
    }

    /// Write a terminal element to (i.e. `<tag> token </tag>`)
    pub fn write_terminal(&mut self, token: &str, ttype: &str)
        -> io::Result<()>
    {
        let token = replace_chars(token);
        let line = format!("{}{} {} {}\n",
                           self.indent(), tag(ttype), token, tag_end(ttype));
        self.file.write_all(&line.as_bytes())
    }

    /// Write a non-terminal opening tag (i.e. `<tag>`)
    pub fn write_nonterm_open(&mut self, ttype: &str)
        -> io::Result<()>
    {
        let line = format!("{}{}\n", self.indent(), tag(ttype));
        self.indent_level += 1;
        self.file.write_all(&line.as_bytes())
    }

    /// Write a non-terminal closing tag (i.e. `</tag>`)
    pub fn write_nonterm_close(&mut self, ttype: &str)
        -> io::Result<()>
    {
        self.indent_level -= 1;
        let line = format!("{}{}\n", self.indent(), tag_end(ttype));
        self.file.write_all(&line.as_bytes())
    }

    /// Get current indentation
    fn indent(&self) -> String {
        let mut indentation = String::new();
        for _ in 0..self.indent_level {
            indentation.push_str(self.indent_str);
        }
        indentation
    }
}

// Helper functions

/// Create an XML tag from a string slice
fn tag(s: &str) -> String {
    ["<", s, ">"].concat()
}

/// Create an XML tag ending from a string slice
fn tag_end(s: &str) -> String {
    ["</", s, ">"].concat()
}

/// Replace '<', '>', '"' and '&' chars by XML-compatible code (i.e. "&lt")
fn replace_chars(s: &str) -> String {
    s.replace("&", "&amp;") // & needs to be replaced first
        .replace(">", "&gt;")
        .replace("\"", "&quot;")
        .replace("<", "&lt;")
}
