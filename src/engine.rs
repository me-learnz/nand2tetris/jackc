//! Compilation Engine module
//!
//! Converts a stream of tokens into a stream of terminals and non-terminals
//! representing an ctual program structure. Engine assumes the input tokens
//! are valid.

use std::collections::HashMap;
use std::fmt;
use std::fs::File;
use std::io::Write;
use std::iter::Peekable;

use symbol_table::{SymbolTable, Kind};
use tokenizer::Tokenizer;
use types::{Token, NonTerm, TokenType};
use types::TokenType::*;
use vm_writer::{VMWriter, Segment, Command};
use xml_writer::XMLWriter;

/// Vectors of choices of common `expect` patterns
lazy_static! {
    static ref CH_ID: Vec<(TokenType, Option<&'static str>)> = [
        (Identifier, None),
    ].iter().cloned().collect();

    static ref CH_TYPE: Vec<(TokenType, Option<&'static str>)> = [
        (Keyword, Some("int")),
        (Keyword, Some("char")),
        (Keyword, Some("boolean")),
        (Identifier, None),
    ].iter().cloned().collect();

    static ref CH_SUBR_TYPE: Vec<(TokenType, Option<&'static str>)> = [
        (Keyword, Some("void")),
        (Keyword, Some("int")),
        (Keyword, Some("char")),
        (Keyword, Some("boolean")),
        (Identifier, None),
    ].iter().cloned().collect();

    static ref CH_STATEMENT: Vec<(TokenType, Option<&'static str>)> = [
        (Keyword, Some("let")),
        (Keyword, Some("if")),
        (Keyword, Some("while")),
        (Keyword, Some("do")),
        (Keyword, Some("return")),
    ].iter().cloned().collect();

    static ref CH_EXPR_ALL: Vec<(TokenType, Option<&'static str>)> = [
        (IntConst, None),
        (StringConst, None),
        (Keyword, Some("true")),
        (Keyword, Some("false")),
        (Keyword, Some("null")),
        (Keyword, Some("this")),
        (Identifier, None),
        (Symbol, Some("(")),
        (Symbol, Some("-")),
        (Symbol, Some("~")),
    ].iter().cloned().collect();

    static ref CH_OP: Vec<(TokenType, Option<&'static str>)> = [
        (Symbol, Some("+")),
        (Symbol, Some("-")),
        (Symbol, Some("*")),
        (Symbol, Some("/")),
        (Symbol, Some("&")),
        (Symbol, Some("|")),
        (Symbol, Some("<")),
        (Symbol, Some(">")),
        (Symbol, Some("=")),
    ].iter().cloned().collect();

    static ref CH_UNARY_OP: Vec<(TokenType, Option<&'static str>)> = [
        (Symbol, Some("-")),
        (Symbol, Some("~")),
    ].iter().cloned().collect();

    static ref CH_CONST: Vec<(TokenType, Option<&'static str>)> = [
        (IntConst, None),
        (StringConst, None),
        (Keyword, Some("true")),
        (Keyword, Some("false")),
        (Keyword, Some("null")),
        (Keyword, Some("this")),
    ].iter().cloned().collect();
}

pub struct Engine {
    /// Iterator supplying tokens
    tokenizer: Peekable<Tokenizer>,
    /// Writer to output the VM commands
    vm_writer: VMWriter,
    /// Optional writer to output the token stream into an XML file
    xml_writer: Option<XMLWriter>,
    /// Symbol table to manage the identifiers
    symbol_table: SymbolTable,
    /// Line number of the last advanced token (first line is 1, init to 0)
    line_num: u32,
    /// (Optional) File for writing the encountered identifiers
    id_file: Option<File>,
    /// Name of the currently processed class
    class: String,
    /// Name of the currently processed subroutine
    subroutine: String,
    /// Various label counters (while, if), reset every subroutine
    cnt: HashMap<&'static str, u16>,
}

impl Engine {
    /// Create and initialize a new Engine
    pub fn new(tokenizer: Tokenizer, vm_writer: VMWriter,
               xml_writer: Option<XMLWriter>, id_file: Option<File>)
        -> Engine
    {
        let tokenizer = tokenizer.peekable();
        Engine {
            tokenizer,
            vm_writer,
            xml_writer,
            symbol_table: SymbolTable::new(),
            line_num: 0,
            id_file,
            class: String::new(),
            subroutine: String::new(),
            cnt: HashMap::new(),
        }
    }

    /// Get a token from tokenizer, match it against `expect` and return the
    /// token if successful (optionally write XML).
    ///
    /// Panics if:
    ///   * Unexpected end of token stream is encountered
    ///   * Token is invalid (error from tokenizer.next())
    ///   * Token does not match any of the `expect` options
    fn advance_token(&mut self, expect: &Vec<(TokenType, Option<&str>)>)
        -> Token
    {
        let (token, line_num) = match self.tokenizer.next() {
            Some(result) => result,
            None => panic!("Encountered an end of file during an unfinished \
                           compilation.")
        };

        self.line_num = line_num + 1;

        if token_matches(&token, expect) {
            self.xml_write_terminal(&token);
        } else {
            // Generate error message
            let mut err = format!("Line {}: Unexpected {}. Expected: ",
                                  self.line_num, token.to_string());
            let mut exp: Vec<String> = Vec::new();
            for &(ref ttype, ref val_opt) in expect {
                let string =  match val_opt {
                    &Some(val) => {
                        vec![ttype.to_string(), val.to_string()].join(" ")
                    }
                    &None => ttype.to_string()
                };
                exp.push(string);
            }
            err.push_str(&exp.join(", "));
            err.push_str(".");
            panic!(err);
        }

        token
    }

    /// Preview the next token and match it against `expect`
    fn peek_matches(&mut self, expect: &Vec<(TokenType, Option<&str>)>) -> bool
    {
        let &(ref token, _) = match self.tokenizer.peek() {
            Some(result) => result,
            None => panic!("Encountered an end of file during an unfinished \
                           compilation.")
        };

        token_matches(&token, &expect)
    }

    /// Write an opening of a given non-term into a XML file
    fn xml_write_open(&mut self, non_term: NonTerm) {
        if let Some(ref mut writer) = self.xml_writer {
            writer.write_nonterm_open(&non_term.to_string())
                .expect(&format!("Error writing an opening tag for `{}` to a \
                                XML file", non_term.to_string()));
        }
    }

    /// Write an closing of a given non-term into a XML file
    fn xml_write_close(&mut self, non_term: NonTerm) {
        if let Some(ref mut writer) = self.xml_writer {
            writer.write_nonterm_close(&non_term.to_string())
                .expect(&format!("Error writing a closing tag for `{}` to a \
                                XML file", non_term.to_string()));
        }
    }

    /// Write a token terminal into a XML file
    fn xml_write_terminal(&mut self, token: &Token) {
        if let Some(ref mut writer) = self.xml_writer {
            let tok_str = token.to_string();
            let v: Vec<&str> = tok_str.splitn(2, ' ').collect();
            writer.write_terminal(v[1], v[0])
                .expect(&format!("Error writing a terminal tag for `{}` to a \
                                XML file", tok_str));
        }
    }

    /// Write an encountered ID into a file in a following format:
    ///   line | action | category | name | (symbol table index)
    /// index can be:
    ///   "0", "1", ... - index from the symbol table
    ///   "nd" - not defined (category is var/arg/static/field but ID is not
    ///          in the symbol table)
    ///   not present - when category is not var/arg/static/field
    fn write_id(&mut self, name: &str, action: IDAction, category: &str) {
        if let Some(ref mut file) = self.id_file {
            let line = match category {
                "var" | "arg" | "static" | "field" => {
                    let idx_str = match self.symbol_table.index_of(name) {
                        Some(idx) => idx.to_string(),
                        None => "nd".to_string()
                    };
                    format!("Line {} {} {} `{}` with index {}.\n",
                            self.line_num, action, category, name, idx_str)
                },
                "class" | "subroutine" => {
                    if let IDAction::Defined = action {
                        if category == "subroutine" {
                            file.write_all("\n".as_bytes()).unwrap();
                        }
                    }
                    format!("Line {} {} {} `{}`.\n",
                            self.line_num, action, category, name)
                },
                _ => panic!("Unknown identifier category {}.", category),
            };

            file.write_all(&line.as_bytes()).unwrap();
        }
    }

    /// Compile a class. Top-level method for starting the compilation.
    pub fn compile_class(&mut self) {
        self.xml_write_open(NonTerm::Class);
        self.advance_token(&vec![(Keyword, Some("class"))]);
        let id_name = self.advance_token(&CH_ID).value;
        self.write_id(&id_name, IDAction::Defined, "class");
        self.class = id_name;
        self.advance_token(&vec![(Symbol, Some("{"))]);
        while self.peek_matches(&vec![
            (Keyword, Some("static")),
            (Keyword, Some("field"))])
        {
            self.compile_class_var_dec();
        }
        while self.peek_matches(&vec![
            (Keyword, Some("constructor")),
            (Keyword, Some("function")),
            (Keyword, Some("method"))])
        {
            self.compile_subroutine_dec();
        }
        self.advance_token(&vec![(Symbol, Some("}"))]);
        self.xml_write_close(NonTerm::Class);

        // Test if there is anything else in the file (this also writes the
        // closing </tokens> tag).
        if let Some(_) = self.tokenizer.next() {
            panic!("Encountered a token after compiling a class in the same \
                   file.");
        }
    }

    /// Compile ClassVarDec
    fn compile_class_var_dec(&mut self) {
        self.xml_write_open(NonTerm::ClassVarDec);
        let category = self.advance_token(&vec![
            (Keyword, Some("static")),
            (Keyword, Some("field")),
        ]).value;
        let id_type = self.advance_token(&CH_TYPE).value;
        let id_name = self.advance_token(&CH_ID).value;
        self.symbol_table.define(&id_name, &id_type, &category);
        self.write_id(&id_name, IDAction::Defined, &category);
        while self.peek_matches(&vec![(Symbol, Some(","))]) {
            self.advance_token(&vec![(Symbol, Some(","))]);
            let id_name = self.advance_token(&CH_ID).value;
            self.symbol_table.define(&id_name, &id_type, &category);
            self.write_id(&id_name, IDAction::Defined, &category);
        }
        self.advance_token(&vec![(Symbol, Some(";"))]);
        self.xml_write_close(NonTerm::ClassVarDec);
    }

    /// Compile SubroutineDec
    fn compile_subroutine_dec(&mut self) {
        self.symbol_table.start_subroutine();
        self.cnt.clear();
        self.xml_write_open(NonTerm::SubroutineDec);
        let subr_type = self.advance_token(&vec![
            (Keyword, Some("constructor")),
            (Keyword, Some("function")),
            (Keyword, Some("method")),
        ]).value;
        let token = self.advance_token(&CH_SUBR_TYPE);
        if let Identifier = token.ttype {
            self.write_id(&token.value, IDAction::Used, "class");
        }
         // In case of method, the first arg is the class itself
        if subr_type == "method" {
            self.symbol_table.define("dummy", &self.class, "arg");
        }
        let id_name = self.advance_token(&CH_ID).value;
        self.write_id(&id_name, IDAction::Defined, "subroutine");
        self.subroutine = id_name;
        self.advance_token(&vec![(Symbol, Some("("))]);
        self.compile_parameter_list();
        self.advance_token(&vec![(Symbol, Some(")"))]);
        self.compile_subroutine_body(&subr_type);
        self.xml_write_close(NonTerm::SubroutineDec);
    }

    /// Compile ParameterList
    fn compile_parameter_list(&mut self) {
        self.xml_write_open(NonTerm::ParameterList);
        if self.peek_matches(&CH_TYPE) {
            let token = self.advance_token(&CH_TYPE);
            if let Identifier = token.ttype {
                self.write_id(&token.value, IDAction::Used, "class");
            }
            let id_name = self.advance_token(&CH_ID).value;
            self.symbol_table.define(&id_name, &token.value, "arg");
            self.write_id(&id_name, IDAction::Defined, "arg");
            while self.peek_matches(&vec![(Symbol, Some(","))]) {
                self.advance_token(&vec![(Symbol, Some(","))]);
                let token = self.advance_token(&CH_TYPE);
                if let Identifier = token.ttype {
                    self.write_id(&token.value, IDAction::Used, "class");
                }
                let id_name = self.advance_token(&CH_ID).value;
                self.symbol_table.define(&id_name, &token.value, "arg");
                self.write_id(&id_name, IDAction::Defined, "arg");
            }
        }
        self.xml_write_close(NonTerm::ParameterList);
    }

    /// Compile SubroutineBody
    fn compile_subroutine_body(&mut self, subr_type: &str) {
        self.xml_write_open(NonTerm::SubroutineBody);
        self.advance_token(&vec![(Symbol, Some("{"))]);
        while self.peek_matches(&vec![(Keyword, Some("var"))]) {
            self.compile_var_dec();
        }
        self.vm_writer
            .write_function(&self.class, &self.subroutine,
                            self.symbol_table.var_count(&Kind::Var))
            .unwrap();
        match subr_type {
            "constructor" => {
                self.vm_writer
                    .write_push(&Segment::Const,
                                self.symbol_table.var_count(&Kind::Field));
                self.vm_writer.write_call("Memory", "alloc", 1);
                self.vm_writer.write_pop(&Segment::Pointer, 0); // pop this
            },
            "method" => {
                self.vm_writer.write_push(&Segment::Arg, 0);
                self.vm_writer.write_pop(&Segment::Pointer, 0); // pop this
            },
            _ => ()
        }
        self.compile_statements();
        self.advance_token(&vec![(Symbol, Some("}"))]);
        self.xml_write_close(NonTerm::SubroutineBody);
    }

    /// Compile VarDec
    fn compile_var_dec(&mut self) {
        self.xml_write_open(NonTerm::VarDec);
        self.advance_token(&vec![(Keyword, Some("var"))]);
        let token = self.advance_token(&CH_TYPE);
        if let Identifier = token.ttype {
            self.write_id(&token.value, IDAction::Used, "class");
        }
        let id_name = self.advance_token(&CH_ID).value;
        self.symbol_table.define(&id_name, &token.value, "var");
        self.write_id(&id_name, IDAction::Defined, "var");
        while self.peek_matches(&vec![(Symbol, Some(","))]) {
            self.advance_token(&vec![(Symbol, Some(","))]);
            let id_name = self.advance_token(&CH_ID).value;
            self.symbol_table.define(&id_name, &token.value, "var");
            self.write_id(&id_name, IDAction::Defined, "var");
        }
        self.advance_token(&vec![(Symbol, Some(";"))]);
        self.xml_write_close(NonTerm::VarDec);
    }

    /// Compile Statements
    fn compile_statements(&mut self) {
        self.xml_write_open(NonTerm::Statements);
        while self.peek_matches(&CH_STATEMENT) {
            if self.peek_matches(&vec![(Keyword, Some("let"))]) {
                self.compile_let_statement();
            } else if self.peek_matches(&vec![(Keyword, Some("if"))]) {
                self.compile_if_statement();
            } else if self.peek_matches(&vec![(Keyword, Some("while"))]) {
                self.compile_while_statement();
            } else if self.peek_matches(&vec![(Keyword, Some("do"))]) {
                self.compile_do_statement();
            } else if self.peek_matches(&vec![(Keyword, Some("return"))]) {
                self.compile_return_statement();
            } else {
                panic!("Starting a statement with something weird.");
            }
        }
        self.xml_write_close(NonTerm::Statements);
    }

    /// Compile LetStatement
    fn compile_let_statement(&mut self) {
        let mut array = false;
        self.xml_write_open(NonTerm::LetStatement);
        self.advance_token(&vec![(Keyword, Some("let"))]);
        let id_name = self.advance_token(&CH_ID).value;
        let kind = self.symbol_table.kind_of(&id_name)
            .expect(&format!("Line {}: Identifier `{}` is not declared.",
                             self.line_num, id_name));
        let idx = self.symbol_table.index_of(&id_name)
            .expect(&format!("Line {}: Identifier `{}` is not declared.",
                             self.line_num, id_name));
        self.write_id(&id_name, IDAction::Used, &kind.to_string());
        if self.peek_matches(&vec![(Symbol, Some("["))]) {
            array = true;
            self.advance_token(&vec![(Symbol, Some("["))]);
            self.compile_expression();
            self.vm_writer.write_push(&Segment::from_kind(&kind), idx);
            self.vm_writer.write_arith(&Command::Add);
            self.advance_token(&vec![(Symbol, Some("]"))]);
        }
        self.advance_token(&vec![(Symbol, Some("="))]);
        self.compile_expression();
        self.advance_token(&vec![(Symbol, Some(";"))]);
        self.xml_write_close(NonTerm::LetStatement);
        if array {  // write result to `that`
            self.vm_writer.write_pop(&Segment::Temp, 0);
            self.vm_writer.write_pop(&Segment::Pointer, 1);
            self.vm_writer.write_push(&Segment::Temp, 0);
            self.vm_writer.write_pop(&Segment::That, 0);
        } else {  // write result to the variable
            self.vm_writer.write_pop(&Segment::from_kind(&kind), idx);
        }
    }

    /// Compile IfStatement
    fn compile_if_statement(&mut self) {
        let count = self.incr_cnt("if").to_string();
        let label_true = "IF_TRUE".to_owned() + &count;
        let label_false = "IF_FALSE".to_owned() + &count;
        let label_end = "IF_END".to_owned() + &count;
        self.xml_write_open(NonTerm::IfStatement);
        self.advance_token(&vec![(Keyword, Some("if"))]);
        self.advance_token(&vec![(Symbol, Some("("))]);
        self.compile_expression();
        self.advance_token(&vec![(Symbol, Some(")"))]);
        self.vm_writer.write_arith(&Command::Not);
        self.vm_writer.write_if_goto(&label_false);
        self.advance_token(&vec![(Symbol, Some("{"))]);
        self.compile_statements();
        self.advance_token(&vec![(Symbol, Some("}"))]);
        self.vm_writer.write_goto(&label_end);
        self.vm_writer.write_label(&label_false);
        if self.peek_matches(&vec![(Keyword, Some("else"))]) {
            self.advance_token(&vec![(Keyword, Some("else"))]);
            self.advance_token(&vec![(Symbol, Some("{"))]);
            self.compile_statements();
            self.advance_token(&vec![(Symbol, Some("}"))]);
        }
        self.xml_write_close(NonTerm::IfStatement);
        self.vm_writer.write_label(&label_end);
    }

    /// Compile WhileStatement
    fn compile_while_statement(&mut self) {
        let count = self.incr_cnt("while").to_string();
        let label_start = "WHILE_EXP".to_owned() + &count;
        let label_end = "WHILE_END".to_owned() + &count;
        self.vm_writer.write_label(&label_start);
        self.xml_write_open(NonTerm::WhileStatement);
        self.advance_token(&vec![(Keyword, Some("while"))]);
        self.advance_token(&vec![(Symbol, Some("("))]);
        self.compile_expression();
        self.advance_token(&vec![(Symbol, Some(")"))]);
        self.vm_writer.write_arith(&Command::Not);
        self.vm_writer.write_if_goto(&label_end);
        self.advance_token(&vec![(Symbol, Some("{"))]);
        self.compile_statements();
        self.advance_token(&vec![(Symbol, Some("}"))]);
        self.xml_write_close(NonTerm::WhileStatement);
        self.vm_writer.write_goto(&label_start);
        self.vm_writer.write_label(&label_end);
    }

    /// Compile DoStatement
    fn compile_do_statement(&mut self) {
        self.xml_write_open(NonTerm::DoStatement);
        self.advance_token(&vec![(Keyword, Some("do"))]);
        let id_name = self.advance_token(&CH_ID).value;
        let category = match self.symbol_table.kind_of(&id_name) {
            Some(kind) => kind.to_string(),
            None => "class".to_string()
        };
        self.write_id(&id_name, IDAction::Used, &category);
        self.subcompile_subroutine_call(&id_name);
        // Get rid of the return value
        self.vm_writer.write_pop(&Segment::Temp, 0);
        self.advance_token(&vec![(Symbol, Some(";"))]);
        self.xml_write_close(NonTerm::DoStatement);
    }

    /// Compile ReturnStatement
    fn compile_return_statement(&mut self) {
        self.xml_write_open(NonTerm::ReturnStatement);
        self.advance_token(&vec![(Keyword, Some("return"))]);
        if self.peek_matches(&CH_EXPR_ALL) {
            self.compile_expression();
        } else {
            self.vm_writer.write_push(&Segment::Const, 0);
        }
        self.advance_token(&vec![(Symbol, Some(";"))]);
        self.vm_writer.write_return();
        self.xml_write_close(NonTerm::ReturnStatement);
    }

    /// Compile Expression
    fn compile_expression(&mut self) {
        self.xml_write_open(NonTerm::Expression);
        self.compile_term();
        while self.peek_matches(&CH_OP) {
            let op = self.advance_token(&CH_OP);
            self.compile_term();
            match op.value.as_str() {
                "+" => self.vm_writer.write_arith(&Command::Add),
                "-" => self.vm_writer.write_arith(&Command::Sub),
                "*" => self.vm_writer.write_call("Math", "multiply", 2),
                "/" => self.vm_writer.write_call("Math", "divide", 2),
                "&" => self.vm_writer.write_arith(&Command::And),
                "|" => self.vm_writer.write_arith(&Command::Or),
                "<" => self.vm_writer.write_arith(&Command::Lt),
                ">" => self.vm_writer.write_arith(&Command::Gt),
                "=" => self.vm_writer.write_arith(&Command::Eq),
                _ => panic!("Wrong op!"),
            }.unwrap();
        }
        self.xml_write_close(NonTerm::Expression);
    }

    /// Compile Term
    fn compile_term(&mut self) {
        self.xml_write_open(NonTerm::Term);
        if self.peek_matches(&CH_CONST) {
            let token = self.advance_token(&CH_CONST);
            match token.ttype {
                IntConst => {
                    let integer = token.value.parse::<u16>().unwrap();
                    self.vm_writer.write_push(&Segment::Const, integer);
                },
                Keyword => {
                    match token.value.as_str() {
                        "true" => {
                            self.vm_writer.write_push(&Segment::Const, 0)
                                .unwrap();
                            self.vm_writer.write_arith(&Command::Not)
                        },
                        "false" | "null" => {
                            self.vm_writer.write_push(&Segment::Const, 0)
                        },
                        "this" => {
                            self.vm_writer.write_push(&Segment::Pointer, 0)
                        },
                        _ => panic!("Line {}: Invalid keyword {}.",
                                    self.line_num, token.value),
                    }.unwrap();
                },
                StringConst => {
                    self.vm_writer.write_string(&token.value).unwrap();
                },
                _ => panic!("{} shouldn't be here!", token.ttype)
            }
        } else if self.peek_matches(&CH_ID) {
            let id_name = self.advance_token(&CH_ID).value;
            let category = match self.symbol_table.kind_of(&id_name) {
                Some(kind) => {
                    let idx = self.symbol_table.index_of(&id_name)
                        .expect(&format!("Line {}: Identifier `{}` is not \
                                         declared.", self.line_num, &id_name));
                    self.vm_writer.write_push(&Segment::from_kind(&kind), idx);
                    kind.to_string()
                },
                None => "class".to_string()
            };
            self.write_id(&id_name, IDAction::Used, &category);
            if self.peek_matches(&vec![(Symbol, Some("["))]) {
                self.advance_token(&vec![(Symbol, Some("["))]);
                self.compile_expression();
                self.vm_writer.write_arith(&Command::Add);
                self.vm_writer.write_pop(&Segment::Pointer, 1);
                self.vm_writer.write_push(&Segment::That, 0);
                self.advance_token(&vec![(Symbol, Some("]"))]);
            } else if self.peek_matches(&vec![(Symbol, Some("(")),
                                              (Symbol, Some("."))]) {
                self.subcompile_subroutine_call(&id_name);
            }
        } else if self.peek_matches(&vec![(Symbol, Some("("))]) {
            self.advance_token(&vec![(Symbol, Some("("))]);
            self.compile_expression();
            self.advance_token(&vec![(Symbol, Some(")"))]);
        } else if self.peek_matches(&CH_UNARY_OP) {
            let op = self.advance_token(&CH_UNARY_OP);
            self.compile_term();
            match op.value.as_str() {
                "-" => self.vm_writer.write_arith(&Command::Neg),
                "~" => self.vm_writer.write_arith(&Command::Not),
                _ => panic!("Wrong op!"),
            }.unwrap();
        }
        self.xml_write_close(NonTerm::Term);
    }

    /// Compile ExpressionList (returns number of arguments)
    fn compile_expression_list(&mut self) -> u16 {
        let mut nargs = 0;
        self.xml_write_open(NonTerm::ExpressionList);
        if self.peek_matches(&CH_EXPR_ALL) {
            self.compile_expression();
            nargs += 1;
            while self.peek_matches(&vec![(Symbol, Some(","))]) {
                self.advance_token(&vec![(Symbol, Some(","))]);
                self.compile_expression();
                nargs += 1;
            }
        }
        self.xml_write_close(NonTerm::ExpressionList);
        nargs
    }

    /// Helper compile subroutineCall (the first ID is passed as argument)
    fn subcompile_subroutine_call(&mut self, id: &str) {
        let (mut class, mut subroutine, mut nargs); // call class.subr nargs
        // function or constructor or external method
        if self.peek_matches(&vec![(Symbol, Some("."))]) {
            self.advance_token(&vec![(Symbol, Some("."))]);
            let subr_name = self.advance_token(&CH_ID).value;
            self.write_id(&subr_name, IDAction::Used, "subroutine");
            subroutine = subr_name.to_string();
            match self.symbol_table.kind_of(id) {
                Some(ref kind) => { // constructed object if present in s.table
                    let segment = Segment::from_kind(kind);
                    let idx = self.symbol_table.index_of(id).unwrap();
                    self.vm_writer.write_push(&segment, idx);
                    class = self.symbol_table.type_of(id).unwrap();
                    nargs = 1;
                },
                None => {
                    class = id.to_string();
                    nargs = 0;
                }
            }
        // method of this
        } else {
            class = self.class.to_string();
            subroutine = id.to_string();
            nargs = 1;
            self.vm_writer.write_push(&Segment::Pointer, 0);  // push this
        }
        self.advance_token(&vec![(Symbol, Some("("))]);
        nargs += self.compile_expression_list();
        self.vm_writer.write_call(&class, &subroutine, nargs);
        self.advance_token(&vec![(Symbol, Some(")"))]);
    }

    /// Increment a label counter of a key (init to 0 if not present)
    /// Return the new value
    fn incr_cnt(&mut self, key: &'static str) -> u16 {
        let count = self.cnt.entry(key)
            .and_modify(|val| { *val += 1 })
            .or_insert(0);
        *count
    }
}

/// Tells whether an identifier was used or defined
enum IDAction {
    Defined,
    Used
}

impl fmt::Display for IDAction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::IDAction::*;
        let string = match *self {
            Defined => "defined",
            Used    => "used",
        };
        write!(f, "{}", string)
    }
}

/// See whether a token matches any given type and/or value.
///
/// `expect` is a vector of tuples (token type, optional value). When the
/// value is None, it accepts any value, otherwise `token` must match both
/// the type and value. If an empty vector is passed, return true.
fn token_matches(token: &Token,
                 expect: &Vec<(TokenType, Option<&str>)>) -> bool
{
    if expect.is_empty() {
        return true;
    }

    // If a `expect` contains a match for `token`, return true.
    for &(ref ttype, ref val_opt) in expect {
        if &token.ttype == ttype {
            match val_opt {
                &Some(val) => {
                    if &token.value == val {
                        return true;
                    }
                },
                &None => {
                    return true;
                }
            }
        }
    }

    false
}

#[cfg(test)]
mod tests {
    use std::io::Write;
    use tempfile::NamedTempFile;
    use tokenizer::Tokenizer;
    use vm_writer::VMWriter;
    use types::Token;
    use types::TokenType::*;
    use super::{Engine, token_matches};

    fn new_engine(contents: &str) -> Engine {
        let mut tmpfile_in = NamedTempFile::new()
            .expect("Error creating temp file.");
        write!(tmpfile_in, "{}", contents)
			.expect("Error writing to temp file");
        let tokenizer = Tokenizer::new(tmpfile_in.path(), None)
			.expect("Error creating tokenizer");
        let tmpfile_out = NamedTempFile::new()
            .expect("Error creating temp file.");
        let vm_writer = VMWriter::new(tmpfile_out.path())
            .expect("Error creating a VMWriter.");
		Engine::new(tokenizer, vm_writer, None, None)
    }

	#[test]
	fn new_engine_creation_peek() {
		let mut engine = new_engine("\
            class Bagr {
            }
        ");
        assert!(engine.peek_matches(&vec![(Keyword, Some("class"))]));
	}

    #[test]
    fn matching_tokens() {
        assert!(token_matches(&Token::new(Keyword, "foo"),
                              &vec![(Keyword, Some("foo"))]));
        assert!(token_matches(&Token::new(Symbol, "foo"),
                              &vec![(Symbol, None)]));
        assert!(token_matches(&Token::new(Identifier, "foo"),
                              &Vec::new()));
        assert!(token_matches(&Token::new(IntConst, "123"),
                              &vec![(Identifier,  Some("bagr")),
                                    (StringConst, Some("bar")),
                                    (IntConst,    Some("321")),
                                    (IntConst,    None)]));
        assert!(!token_matches(&Token::new(StringConst, "foo"),
                               &vec![(StringConst, Some("bar"))]));
    }

    #[test]
    #[should_panic(expected="Encountered a token after compiling a class in \
                            the same file.")]
    fn err_advance_token_eof() {
		let mut engine = new_engine("\
            class Bagr {
            }
            class
        ");
        engine.compile_class();
    }

    #[test]
    #[should_panic(expected="Encountered a token after compiling a class in \
                            the same file.")]
    fn err_advance_token_invalid() {
		let mut engine = new_engine("\
            class Bagr {
            }
            class
        ");
        engine.compile_class();
    }

    #[test]
    #[should_panic(expected="Line 2: Unexpected keyword int. Expected: symbol \
                            }.")]

    fn err_advance_token_nomatch() {
		let mut engine = new_engine("\
            class Bagr {
               int foo;
            }
        ");
        engine.compile_class();
    }
}
