/*!
Symbol table for storing the context of used identifiers.
*/

use std::fmt;
use std::collections::HashMap;

pub struct SymbolTable {
    class_table: HashMap<String, Context>,
    subroutine_table: HashMap<String, Context>,
    running_idx: HashMap<Kind, u16>,
    in_subroutine: bool,
}

impl SymbolTable {
    /// Create a new empty SymbolTable and start is in the class scope
    pub fn new() -> SymbolTable {
        let mut running_idx = HashMap::new();
        running_idx.insert(Kind::Static, 0);
        running_idx.insert(Kind::Field, 0);
        running_idx.insert(Kind::Arg, 0);
        running_idx.insert(Kind::Var, 0);

        SymbolTable {
            class_table: HashMap::new(),
            subroutine_table: HashMap::new(),
            running_idx,
            in_subroutine: false,
        }
    }

    /// Start a new subroutine (=> reset the current subroutine table)
    pub fn start_subroutine(&mut self) {
        self.in_subroutine = true;
        self.subroutine_table.clear();
        self.running_idx.insert(Kind::Arg, 0);
        self.running_idx.insert(Kind::Var, 0);
    }

    /// Add a new identifier to the SymbolTable
    pub fn define(&mut self, name: &str, id_type: &str, kind: &str) {
        let kind = Kind::new(kind).unwrap();

        let idx = match self.running_idx.get(&kind) {
            Some(num) => *num,
            None => panic!("Error retrieving index from {:?}.", kind)
        };
        self.running_idx.insert(kind.clone(), idx+1);

        let context = Context::new(id_type, kind, idx);

        let id_present = match context.kind {
            Kind::Static | Kind::Field => {
                if self.in_subroutine {
                    panic!("Wrong kind of identifier `{}`. Static and Field \
                           can be declared only in a class.", name)
                } else {
                    self.class_table.insert(name.to_string(), context)
                }
            },
            Kind::Arg | Kind::Var => {
                if self.in_subroutine {
                    self.subroutine_table.insert(name.to_string(), context)
                } else {
                    panic!("Wrong kind of identifier `{}`. Arg and Var \
                           can be declared only in a subroutine.", name)
                }
            }
        };

        if let Some(_) = id_present {
            panic!("Identifier `{}` is already present in the symbol table",
                   name);
        }
    }

    /// Count variables of a given kind in the current scope
    pub fn var_count(&self, kind: &Kind) -> u16 {
        let mut count = 0;
        let iter = match kind {
            &Kind::Static | &Kind::Field => {
                self.class_table.values()
            },
            &Kind::Arg | &Kind::Var => {
                self.subroutine_table.values()
            }
        };

        for context in iter {
            if &context.kind == kind {
                count += 1;
            }
        }

        count
    }

    /// Returns the kind of a variable of a given name. None if not present.
    pub fn kind_of(&self, name: &str) -> Option<Kind> {
        match self.find_context(name) {
            Some(context) => Some(context.kind.clone()),
            None => None
        }
    }

    /// Returns the type of a variable of a given name. None if not present.
    pub fn type_of(&self, name: &str) -> Option<String> {
        match self.find_context(name) {
            Some(context) => Some(context.id_type.clone()),
            None => None
        }
    }

    /// Returns the index of a variable of a given name. None if not present.
    pub fn index_of(&self, name: &str) -> Option<u16> {
        match self.find_context(name) {
            Some(context) => Some(context.index),
            None => None
        }
    }

    /// Try to find a context in a current table. If inside a subroutine and
    /// no context found, try the class table. Return None if nothing found.
    fn find_context(&self, name: &str) -> Option<&Context> {
        if self.in_subroutine {
            match self.subroutine_table.get(name) {
                Some(ref context) => Some(context),
                None => {
                    match self.class_table.get(name) {
                        Some(ref context) => Some(context),
                        None => None
                    }
                }
            }
        } else {
            match self.class_table.get(name) {
                Some(ref context) => Some(context),
                None => None
            }
        }
    }
}

/// Simple struct storing the info about an identifier: type, kind, index
struct Context {
    id_type: String,
    kind: Kind,
    index: u16,
}

impl Context {
    fn new(id_type: &str, kind: Kind, index: u16) -> Context {
        Context {
            id_type: id_type.to_string(),
            kind,
            index,
        }
    }
}

/// Possible kinds of identifiers in the symbol table
#[derive(Hash, PartialEq, Eq, Debug, Clone)]
pub enum Kind {
    Static,
    Field,
    Arg,
    Var,
}

impl Kind {
    pub fn new(kind: &str) -> Result<Kind, String> {
        use self::Kind::*;
        match kind {
            "static" => Ok(Static),
            "field"  => Ok(Field),
            "arg"    => Ok(Arg),
            "var"    => Ok(Var),
            _        => Err(format!("Unknown identifier kind `{}`", kind)),
        }
    }
}

impl fmt::Display for Kind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Kind::*;
        let string = match *self {
            Static => "static",
            Field  => "field",
            Arg    => "arg",
            Var    => "var",
        };
        write!(f, "{}", string)
    }
}

#[cfg(test)]
mod tests {
    use super::{SymbolTable, Kind};

    #[test]
    fn new_symbol_table() {
        let table = SymbolTable::new();
        table.class_table.is_empty();
        table.subroutine_table.is_empty();
        assert_eq!(table.running_idx.get(&Kind::Static), Some(&0));
        assert_eq!(table.running_idx.get(&Kind::Field), Some(&0));
        assert_eq!(table.running_idx.get(&Kind::Arg), Some(&0));
        assert_eq!(table.running_idx.get(&Kind::Var), Some(&0));
    }

    #[test]
    fn define_new_id() {
        let mut table = SymbolTable::new();
        table.define("foo", "String", "static");
        table.define("bar", "Class", "static");
        table.define("baz", "Bagr", "field");
        table.start_subroutine();
        table.define("foo", "String", "arg");
        table.define("bar", "Class", "arg");
        table.define("baz", "Bagr", "var");
        table.start_subroutine();
        table.define("foo", "String", "arg");
        table.define("bar", "Class", "arg");
        table.define("baz", "Bagr", "var");
        assert_eq!(table.running_idx.get(&Kind::Static), Some(&2));
        assert_eq!(table.running_idx.get(&Kind::Field), Some(&1));
        assert_eq!(table.running_idx.get(&Kind::Arg), Some(&2));
        assert_eq!(table.running_idx.get(&Kind::Var), Some(&1));
    }

    #[test]
    #[should_panic]
    fn err_define_id_exists() {
        let mut table = SymbolTable::new();
        table.define("foo", "String", "static");
        table.define("foo", "int", "static");
    }

    #[test]
    #[should_panic]
    fn err_define_id_wrong_scope_class() {
        let mut table = SymbolTable::new();
        table.define("foo", "String", "var");
    }

    #[test]
    #[should_panic]
    fn err_define_id_wrong_scope_subroutine() {
        let mut table = SymbolTable::new();
        table.start_subroutine();
        table.define("foo", "String", "static");
    }

    #[test]
    fn reading_entries() {
        let mut table = SymbolTable::new();
        table.define("foo", "String", "static");
        table.define("bar", "Class", "static");
        table.define("baz", "Bagr", "field");
        table.define("classonly", "int", "field");

        assert_eq!(table.var_count(&Kind::Var), 0);
        assert_eq!(table.kind_of("foo"), Some(Kind::Static));
        assert_eq!(table.kind_of("bagr"), None);
        assert_eq!(table.type_of("bar"), Some(String::from("Class")));
        assert_eq!(table.type_of("bagr"), None);
        assert_eq!(table.index_of("bar"), Some(1));
        assert_eq!(table.index_of("baz"), Some(0));
        assert_eq!(table.index_of("bagr"), None);

        table.start_subroutine();
        table.define("foo", "String", "arg");
        table.define("bar", "Bar", "arg");
        table.define("baz", "Bagr", "var");

        assert_eq!(table.var_count(&Kind::Static), 2);
        assert_eq!(table.var_count(&Kind::Arg), 2);
        assert_eq!(table.kind_of("foo"), Some(Kind::Arg));
        assert_eq!(table.kind_of("classonly"), Some(Kind::Field));
        assert_eq!(table.type_of("bar"), Some(String::from("Bar")));
        assert_eq!(table.type_of("classonly"), Some(String::from("int")));
        assert_eq!(table.index_of("bar"), Some(1));
        assert_eq!(table.index_of("baz"), Some(0));
        assert_eq!(table.index_of("classonly"), Some(1));
    }
}
