//! Data types used by the program
//!
//! The following types are introduced:
//!   * TokenType (enum)
//!   * Token (struct)
//!   * NonTerm (enum)

use std::fmt;

#[derive(PartialEq, Clone)]
#[cfg_attr(test, derive(Debug))]
pub enum TokenType {
    Keyword,
    Symbol,
    Identifier,
    IntConst,
    StringConst,
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::TokenType::*;
        let string = match *self {
            Keyword     => "keyword",
            Symbol      => "symbol",
            Identifier  => "identifier",
            IntConst    => "integerConstant",
            StringConst => "stringConstant",
        };
        write!(f, "{}", string)
    }
}

#[cfg_attr(test, derive(Debug, PartialEq))]
pub struct Token {
    pub ttype: TokenType,
    pub value: String,
}

impl Token {
    pub fn new(ttype: TokenType, value: &str) -> Token {
        Token {
            ttype: ttype,
            value: String::from(value),
        }
    }
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut string = self.ttype.to_string();
        string.push_str(" ");
        string.push_str(&self.value);
        write!(f, "{}", string)
    }
}

/// All possible non-terminals
#[cfg_attr(test, derive(Debug))]
pub enum NonTerm {
    // Program structure
    Class,
    ClassVarDec,
    SubroutineDec,
    ParameterList,
    SubroutineBody,
    VarDec,
    // Statements
    Statements,
    WhileStatement,
    IfStatement,
    ReturnStatement,
    LetStatement,
    DoStatement,
    // Expressions
    Expression,
    Term,
    ExpressionList,
}

impl fmt::Display for NonTerm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::NonTerm::*;
        let nonterm_str = match *self {
            Class           => "class",
            ClassVarDec     => "classVarDec",
            SubroutineDec   => "subroutineDec",
            ParameterList   => "parameterList",
            SubroutineBody  => "subroutineBody",
            VarDec          => "varDec",
            Statements      => "statements",
            WhileStatement  => "whileStatement",
            IfStatement     => "ifStatement",
            ReturnStatement => "returnStatement",
            LetStatement    => "letStatement",
            DoStatement     => "doStatement",
            Expression      => "expression",
            Term            => "term",
            ExpressionList  => "expressionList",
        };
        write!(f, "{}", nonterm_str)
    }
}
