/*!
Writes VM commands into a file.
*/

use std::fmt;
use std::io;
use std::io::Write;
use std::fs::File;
use std::path::Path;

use symbol_table::Kind;

/// Memory segments
pub enum Segment {
    Const,
    Arg,
    Local,
    Static,
    This,
    That,
    Pointer,
    Temp,
}

impl fmt::Display for Segment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Segment::*;
        let string = match *self {
            Const   => "constant",
            Arg     => "argument",
            Local   => "local",
            Static  => "static",
            This    => "this",
            That    => "that",
            Pointer => "pointer",
            Temp    => "temp",
        };
        write!(f, "{}", string)
    }
}

impl Segment {
    pub fn from_kind(kind: &Kind) -> Segment {
        match kind {
            &Kind::Var    => Segment::Local,
            &Kind::Arg    => Segment::Arg,
            &Kind::Field  => Segment::This,
            &Kind::Static => Segment::Static,
        }
    }
}

/// VM arithmetic commands
pub enum Command {
    Add,
    Sub,
    Neg,
    Eq,
    Gt,
    Lt,
    And,
    Or,
    Not,
}

impl fmt::Display for Command {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::Command::*;
        let string = match *self {
            Add => "add",
            Sub => "sub",
            Neg => "neg",
            Eq  => "eq",
            Gt  => "gt",
            Lt  => "lt",
            And => "and",
            Or  => "or",
            Not => "not",
        };
        write!(f, "{}", string)
    }
}

/// VMWriter
pub struct VMWriter {
    file: File,
}

impl VMWriter {
    /// Create a new VMWriter with a specified file
    pub fn new(file_path: &Path) -> io::Result<VMWriter>
    {
        let file = File::create(file_path)?;
        Ok(VMWriter { file })
    }

    /// Write a push command
    pub fn write_push(&mut self, segment: &Segment, index: u16)
        -> io::Result<()>
    {
        let line = format!("push {} {}\n", segment.to_string(), index);
        self.file.write_all(&line.as_bytes())
    }

    /// Write a pop command
    pub fn write_pop(&mut self, segment: &Segment, index: u16)
        -> io::Result<()>
    {
        let line = format!("pop {} {}\n", segment.to_string(), index);
        self.file.write_all(&line.as_bytes())
    }

    /// Write an arithmetic command
    pub fn write_arith(&mut self, command: &Command)
        -> io::Result<()>
    {
        let line = format!("{}\n", command.to_string());
        self.file.write_all(&line.as_bytes())
    }

    /// Write a label command
    pub fn write_label(&mut self, label: &str)
        -> io::Result<()>
    {
        let line = format!("label {}\n", label);
        self.file.write_all(&line.as_bytes())
    }

    /// Write a goto command
    pub fn write_goto(&mut self, label: &str)
        -> io::Result<()>
    {
        let line = format!("goto {}\n", label);
        self.file.write_all(&line.as_bytes())
    }

    /// Write an if-goto command
    pub fn write_if_goto(&mut self, label: &str)
        -> io::Result<()>
    {
        let line = format!("if-goto {}\n", label);
        self.file.write_all(&line.as_bytes())
    }

    /// Write a call command
    pub fn write_call(&mut self, class: &str, name: &str, nargs: u16)
        -> io::Result<()>
    {
        let line = format!("call {}.{} {}\n", class, name, nargs);
        self.file.write_all(&line.as_bytes())
    }

    /// Write a function command
    pub fn write_function(&mut self, class: &str, name: &str, nlocals: u16)
        -> io::Result<()>
    {
        let line = format!("function {}.{} {}\n", class, name, nlocals);
        self.file.write_all(&line.as_bytes())
    }

    /// Write a return command
    pub fn write_return(&mut self)
        -> io::Result<()>
    {
        self.file.write_all("return\n".as_bytes())
    }

    /// Write a string (composition of push constant and OS calls)
    /// Assumes ASCII encoding (1 byte per char)
    pub fn write_string(&mut self, s: &str)
        -> io::Result<()>
    {
        self.write_push(&Segment::Const, s.len() as u16)?;
        self.write_call("String", "new", 1)?;
        for byte in s.bytes() {
            self.write_push(&Segment::Const, byte as u16)?;
            self.write_call("String", "appendChar", 2)?;
        }
        Ok(())
    }
}
