//! Jack Syntax Compiler reads and decodes the input .jack program file
//!
//! It has the following tasks:
//!   1. Create a token stream and pass it to a new Engine module.
//!   2. Drive the Engine module to perform the compilation.
//!   3. Manage which XML files are getting written to.
//!
//! The Compiler operates on one source .jack file at a time. Optional XML
//! files are created on the fly.

use std::io;
use std::fs::File;
use std::path::Path;

use engine::Engine;
use tokenizer::Tokenizer;
use xml_writer::XMLWriter;
use vm_writer::VMWriter;

/// `Compiler` drives the compilation process
pub struct Compiler {
    engine: Engine,
}

impl Compiler {
    /// Create a new Compiler
    pub fn new(src_file: &Path, out_dir: &Path,
               xml_tokens: bool, xml_tree: bool, write_ids: bool, silent: bool)
        -> io::Result<Compiler>
    {
        let base_name = src_file.file_stem()
            .expect("Error creating file stem from source file")
            .to_str().expect("Error converting &OsStr to &str");

        let out_file = out_dir.join(base_name.to_owned() + ".vm");
        if !silent {
            println!(
                "{} -> {}",
                src_file
                    .file_name().expect("Error getting src filename")
                    .to_str().expect("Error converting src &OsStr to &str"),
                out_file
                    .file_name().expect("Error getting out filename")
                    .to_str().expect("Error converting out &OsStr to &str")
            );
        }

        let xml_dir = out_dir;

        let vm_writer = {
            let fvm_name = base_name.to_owned() + ".vm";
            let fvm = xml_dir.join(fvm_name);
            VMWriter::new(&fvm)?
        };

        let tree_writer = match xml_tree {
            true => {
                let ftree_name = base_name.to_owned() + ".xml";
                let ftree = xml_dir.join(ftree_name);
                Some(XMLWriter::new(&ftree)?)
            },
            false => None,
        };

        let token_writer = match xml_tokens {
            true => {
                let ftoken_name = base_name.to_owned() + "T.xml";
                let ftoken = xml_dir.join(ftoken_name);
                Some(XMLWriter::new(&ftoken)?)
            },
            false => None,
        };

        let id_file = match write_ids {
            true => {
                let fid_name = base_name.to_owned() + "ID.txt";
                let fid = out_dir.join(fid_name);
                Some(File::create(&fid)?)
            },
            false => None,
        };

        let tokenizer = Tokenizer::new(src_file, token_writer)?;
        let engine = Engine::new(tokenizer, vm_writer, tree_writer, id_file);

        Ok(Compiler { engine })
    }

    /// Compile the file
    pub fn compile(&mut self) {
        self.engine.compile_class();
    }
}
