extern crate clap;
#[macro_use]
extern crate lazy_static;
#[cfg(test)]
extern crate tempfile;

use std::fs;
use std::path::PathBuf;

use clap::{App, Arg, ArgMatches};

use compiler::Compiler;

mod compiler;
mod engine;
mod types;
mod tokenizer;
mod xml_writer;
mod symbol_table;
mod vm_writer;

pub fn run() {
    let args = cli_args();

    // CLI flags
    let silent     = args.is_present("silent");
    let xml_tokens = args.is_present("xml-tokens");
    let xml_tree   = args.is_present("xml-tree");
    let write_ids  = args.is_present("write-ids");

    // Absolute path of SOURCE file/dir
    let src_path = PathBuf::from(args.value_of("SOURCE")
        .expect("Error creating a source path from a CLI argument"));
    let src_path = src_path.canonicalize()
        .expect(&format!("Error creating an absolute path from `{}`",
                         src_path.display()));

    // Output directory
    let out_dir = match args.value_of("output") {
        Some(output) => {
            let path = PathBuf::from(output);
            if !path.is_dir() {
                panic!("Output path {} is not a directory.", path.display());
            }
            if !path.exists() {
                panic!("Output directory {} does not exist.", path.display());
            }
            path
        },
        None => {
            let path = match src_path.is_dir() {
                true => &src_path,
                false => src_path.parent().expect("Source path has no parent"),
            };
            PathBuf::from(path)
        }
    };
    let out_dir = out_dir.canonicalize()
        .expect(&format!("Error creating an absolute path from `{}`",
                         out_dir.display()));

    // Create a vector of source files
    let mut src_files: Vec<PathBuf> = Vec::new();

    if src_path.is_dir() {
        let dir_files = fs::read_dir(&src_path)
            .expect("Error reading files in the source directory");
        for src_file in dir_files {
            let src_file = src_file.expect("No source files").path();
            let has_jack_extension = match src_file.extension() {
                Some(extension) => {
                    if extension == "jack" {
                        true
                    } else {
                        false
                    }
                },
                None => false
            };
            if has_jack_extension {
                src_files.push(src_file);
            }
        }
        if src_files.is_empty() {
            panic!("Directory {} does not contain any files with `.jack` \
                    extension!", src_path.display());
        }
    } else {
        src_files.push(src_path.to_path_buf());
    }

    if !silent {
        println!("Output dir: {}\n", &out_dir.display());
        println!("Optional outputs:");
        println!("  XML tokens:            {}", xml_tokens);
        println!("  XML program structure: {}", xml_tree);
        println!("  Identifiers' context:  {}\n", write_ids);
    }
    for src in src_files.iter() {
        let mut compiler = Compiler::new(&src, &out_dir,
                                         xml_tokens, xml_tree, write_ids,
                                         silent).unwrap();
        compiler.compile();
    }
}

fn cli_args<'a>() -> ArgMatches<'a> {
    App::new("jackc")
        .version("0.0.0")
        .author("Jakub Žádník <kubouch@gmail.com>")
        .about("Compiler of a Jack language into a VM language for a Hack \
               computer. Developed while working on www.nand2tetris.org.")
        .arg(Arg::with_name("SOURCE")
            .help("Input source file or a directory containing source files.")
            .required(true)
            .index(1))
        .arg(Arg::with_name("output")
            .short("o")
            .help("Optional output directory")
            .takes_value(true))
        .arg(Arg::with_name("silent")
            .short("s")
            .long("silent")
            .help("Do not print anything (errors are still shown)."))
        .arg(Arg::with_name("xml-tokens")
            .long("xml-tokens")
            .help("Write a flat token stream to an  XML file."))
        .arg(Arg::with_name("xml-tree")
            .long("xml-tree")
            .help("Write an analyzed program structure to an XML file."))
        .arg(Arg::with_name("write-ids")
            .long("write-ids")
            .help("Write encountered identifiers and their context to a \
                  file."))
        .get_matches()
}
