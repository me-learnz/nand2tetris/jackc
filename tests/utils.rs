/*!
This module provides a functions for
  * comparing two files line-by-line
  * creating a new temporary file
*/

extern crate tempfile;

use std::io::{BufReader,BufRead};
use std::io::Write;
use std::fs::File;
use std::path::Path;

use self::tempfile::NamedTempFile;

/// Compare two files line-by-line, optionally ignoring whitespace.
///
/// In case of error, it panics and the error messages prints the mismatched
/// line number.
pub fn compare_files(f1_path: &Path, f2_path: &Path, ignore_ws: bool) {
    let f1 = File::open(f1_path)
        .expect(&format!("Error opening {:?}!", f1_path));
    let f2 = File::open(f2_path)
        .expect(&format!("Error opening {:?}!", f2_path));

    let f1_lines = BufReader::new(f1).lines().enumerate();
    let mut f2_lines = BufReader::new(f2).lines();

    for (i, f1_line) in f1_lines {
        let f2_line = match f2_lines.next() {
            Some(line) => line,
            None => panic!("{:?} finished but {:?} still has some lines.",
                           f2_path, f1_path)
        };

        let mut f1_line = match f1_line {
            Ok(line) => line,
            Err(e) => panic!("Error geting line from {:?}: {:?}", f1_path, e)
        };

        let mut f2_line = match f2_line {
            Ok(line) => line,
            Err(e) => panic!("Error geting line from {:?}: {:?}", f2_path, e)
        };

        if ignore_ws {
            f1_line = f1_line.trim().to_string();
            f2_line = f2_line.trim().to_string();
        }

        if f1_line != f2_line {
            panic!("Error on line {}.\n{:?}:\n`{}`\n{:?}:\n`{}`",
                   i, f1_path, f1_line, f2_path, f2_line);
        }
    }

    if let Some(_) = f2_lines.next() {
        panic!("{:?} finished but {:?} still has some lines.",
               f1_path, f2_path)
    }
}

/// Create a new temp file, write `contents` to it and return it.
pub fn new_tmpfile(contents: &str) -> NamedTempFile {
    let mut tmpfile = NamedTempFile::new()
        .expect("Error creating temp file.");
    write!(tmpfile, "{}", contents)
        .expect("Error writing to temp file");
    tmpfile
}

#[test]
fn test_compare() {
    let contents = "a\nb\nc";
    let f1 = new_tmpfile(contents);
    let f2 = new_tmpfile(contents);
    compare_files(f1.path(), f2.path(), false);
    let contents = "  a  \n b \nc";
    let f3 = new_tmpfile(contents);
    compare_files(f1.path(), f3.path(), true);
}

#[test]
#[should_panic]
fn err_compare_whitespace() {
    let contents = "  a  \n b \nc\n";
    let f1 = new_tmpfile(contents);
    let contents = "a\nb\nc\n";
    let f2 = new_tmpfile(contents);
    compare_files(f1.path(), f2.path(), false);
}

#[test]
#[should_panic]
fn err_compare_f1_more_lines() {
    let contents = "a\nb\nc\n";
    let f1 = new_tmpfile(contents);
    let contents = "a\nb\n";
    let f2 = new_tmpfile(contents);
    compare_files(f1.path(), f2.path(), false);
}

#[test]
#[should_panic]
fn err_compare_f2_more_lines() {
    let contents = "a\nb\n";
    let f1 = new_tmpfile(contents);
    let contents = "a\nb\nc\n";
    let f2 = new_tmpfile(contents);
    compare_files(f1.path(), f2.path(), false);
}
