/*!
Integration tests. Each test compiles a source .jack file and compares it to a
reference output file.

Both source and reference files are in the `data` directory, grouped to folders
by difficulty. Reference files have `_ref` added to their name.
*/

use std::env;
use std::process::Command;
use std::path::PathBuf;

use utils::compare_files;

mod utils;

/// Return PathBuf to the jackc executable (jackc/target/debug/jackc)
fn jackc_exe() -> PathBuf {
    let root = env::current_exe().unwrap()
        .parent().expect("No parent for executable.")
        .parent().expect("No parent for executable.").to_path_buf();
    root.join("jackc")
}

/// Return a Command of the jack executable
fn jackc_cmd() -> Command {
    Command::new(&jackc_exe())
}

/// Return the PathBuf to the jackc/tests/data directory
fn tests_data() -> PathBuf {
    jackc_exe()
        .parent().expect("No parent for executable.")
        .parent().expect("No parent for executable.")
        .parent().expect("No parent for executable.")
        .join("tests").join("data")
}

/// Run a command and wait until it finished
fn run_cmd(cmd: &mut Command) {
    cmd.output()
        .expect(&format!("{:?} failed to start", cmd));
        // .wait()
        // .expect(&format!("{:?} not running", cmd));
}

/// Testing `--help` and empty class
#[test]
fn bare() {
    run_cmd(jackc_cmd().arg("--help"));

    run_cmd(jackc_cmd()
            .arg("--xml-tokens")
            .arg(&tests_data().join("Class/Bare.jack")));

    run_cmd(jackc_cmd()
            .arg("--xml-tree")
            .arg(&tests_data().join("Class/Bare.jack")));

    compare_files(&tests_data().join("Class/BareT_ref.xml"),
                  &tests_data().join("Class/BareT.xml"),
                  false);

    compare_files(&tests_data().join("Class/Bare_ref.xml"),
                  &tests_data().join("Class/Bare.xml"),
                  false);
}

#[test]
fn expression_less_square() {
    run_cmd(jackc_cmd()
            .arg("--xml-tokens").arg("--xml-tree")
            .arg(&tests_data().join("ExpressionLessSquare")));

    compare_files(&tests_data().join("ExpressionLessSquare/MainT_ref.xml"),
                  &tests_data().join("ExpressionLessSquare/MainT.xml"),
                  true);
    compare_files(&tests_data().join("ExpressionLessSquare/SquareGameT_ref.xml"),
                  &tests_data().join("ExpressionLessSquare/SquareGameT.xml"),
                  true);
    compare_files(&tests_data().join("ExpressionLessSquare/SquareT_ref.xml"),
                  &tests_data().join("ExpressionLessSquare/SquareT.xml"),
                  true);
    compare_files(&tests_data().join("ExpressionLessSquare/Main_ref.xml"),
                  &tests_data().join("ExpressionLessSquare/Main.xml"),
                  false);
    compare_files(&tests_data().join("ExpressionLessSquare/SquareGame_ref.xml"),
                  &tests_data().join("ExpressionLessSquare/SquareGame.xml"),
                  false);
    compare_files(&tests_data().join("ExpressionLessSquare/Square_ref.xml"),
                  &tests_data().join("ExpressionLessSquare/Square.xml"),
                  false);
}

#[test]
fn square() {
    run_cmd(jackc_cmd()
            .arg("--xml-tokens").arg("--xml-tree")
            .arg(&tests_data().join("Square")));

    compare_files(&tests_data().join("Square/MainT_ref.xml"),
                  &tests_data().join("Square/MainT.xml"),
                  true);
    compare_files(&tests_data().join("Square/SquareGameT_ref.xml"),
                  &tests_data().join("Square/SquareGameT.xml"),
                  true);
    compare_files(&tests_data().join("Square/SquareT_ref.xml"),
                  &tests_data().join("Square/SquareT.xml"),
                  true);
    compare_files(&tests_data().join("Square/Main_ref.xml"),
                  &tests_data().join("Square/Main.xml"),
                  false);
    compare_files(&tests_data().join("Square/SquareGame_ref.xml"),
                  &tests_data().join("Square/SquareGame.xml"),
                  false);
    compare_files(&tests_data().join("Square/Square_ref.xml"),
                  &tests_data().join("Square/Square.xml"),
                  false);
}

#[test]
fn array() {
    run_cmd(jackc_cmd()
            .arg("--xml-tokens").arg("--xml-tree")
            .arg(&tests_data().join("ArrayTest")));

    compare_files(&tests_data().join("ArrayTest/MainT_ref.xml"),
                  &tests_data().join("ArrayTest/MainT.xml"),
                  true);
    compare_files(&tests_data().join("ArrayTest/Main_ref.xml"),
                  &tests_data().join("ArrayTest/Main.xml"),
                  false);
}
